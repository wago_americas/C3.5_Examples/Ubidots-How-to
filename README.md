# Ubidots/MQTT

## Project Details

- [ ] Written in Structured Text
- [ ] Codesys Version 3.5.17.3
- [ ] Target: PFC200 750-8212 FW24


## Name
MQTT Communicates to the Ubidots Cloud

## Description
This program discusses the implementation and configuration of connecting to a cloud service via MQTT on a PFC200.

# Ubidots-How-to 
![image](https://user-images.githubusercontent.com/42245728/226696161-3a9d5bcb-cff5-4b63-9d31-0594b4dee729.png)
![image](https://user-images.githubusercontent.com/42245728/226696210-9409479b-62f9-46a1-a749-45cde10c62bb.png)

![Add-Device-in-Ubidots](https://user-images.githubusercontent.com/42245728/226706025-95305fde-e1c1-4f62-9931-2c9d5653ebb0.gif)
![image](https://user-images.githubusercontent.com/42245728/226706271-fb7c34b0-c83c-4fe2-9151-244c9e4c3f63.png)
![image](https://user-images.githubusercontent.com/42245728/226706304-869a7539-7766-4860-b87c-b38ba481cd7e.png)

![PFC-Config-in-WBM](https://user-images.githubusercontent.com/42245728/226706857-30720018-2a1d-415f-9ae6-4d51344482ec.gif)

![image](https://user-images.githubusercontent.com/42245728/226708749-00fc0abd-daa4-4e0b-9973-4a363dbeba17.png)


![Confirm-Online-Connection-Via-SSH](https://user-images.githubusercontent.com/42245728/226709216-e5b2ba15-6a56-4605-9cde-495f21f73329.gif)
![image](https://user-images.githubusercontent.com/42245728/226709368-82e413f6-a1dd-4eb0-934a-109f6190369d.png)

![Cloud-Setup-in-WBM](https://user-images.githubusercontent.com/42245728/226709630-45e20226-4647-4234-bd0e-07592e5cc0de.gif)
![image](https://user-images.githubusercontent.com/42245728/226709665-e558fe4e-f306-444d-ab7d-21c640746e9e.png)

![image](https://user-images.githubusercontent.com/42245728/226709753-c4220d16-77d2-4c41-93ca-747c5b217b73.png)
![Add-Device-in-Codesys](https://user-images.githubusercontent.com/42245728/226709921-21d052d6-6d2c-4c92-8d20-e196082925a1.gif)

![image](https://user-images.githubusercontent.com/42245728/226710074-f4275fb2-3167-41f3-9cb8-75685d3a1d2f.png)
![POU-Task-Config](https://user-images.githubusercontent.com/42245728/226710201-d77e98df-f5f3-48a2-8c4e-9f0fa6ca3de6.gif)

![image](https://user-images.githubusercontent.com/42245728/226710227-2ffee711-447a-4a04-8305-414bab8dbe56.png)
![image](https://user-images.githubusercontent.com/42245728/226710252-8d5673b3-28a5-4197-96cf-535c6a66173b.png)
![image](https://user-images.githubusercontent.com/42245728/226710289-4cd6af64-10fa-40ab-badd-41f1109a2367.png)

![Program-Execution](https://user-images.githubusercontent.com/42245728/226710478-9b8b7f41-bb7a-4734-9b91-e1854d4cb1d1.gif)

![image](https://user-images.githubusercontent.com/42245728/226710508-3ecf4663-27b7-49f1-8c0b-4fd12e06079d.png)
![image](https://user-images.githubusercontent.com/42245728/226710525-b93db10f-9f03-44f4-b354-5f5ec7bc30a2.png)
![image](https://user-images.githubusercontent.com/42245728/226710543-321494cb-0527-48f8-9ade-26408fadbea3.png)
![image](https://user-images.githubusercontent.com/42245728/226710562-42abdbd9-038d-4ad5-9066-1055542f54ee.png)

![Final-Program-Execution](https://user-images.githubusercontent.com/42245728/226710750-1e7419c2-d6a5-4233-a69c-cb207a2cab0d.gif)

## Support
This program is for demonstration only and does not include support.  May contain bugs.  Use at your own risk.

## Authors and acknowledgment
Joe Abdelmalak
May 2023

## License
Copyright (c) 2023, Joe Abdelmalak, WAGO

Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:

Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE
